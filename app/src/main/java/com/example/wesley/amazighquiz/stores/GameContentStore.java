package com.example.wesley.amazighquiz.stores;

import android.content.Context;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.models.Theme;
import com.example.wesley.amazighquiz.models.Word;
import com.example.wesley.amazighquiz.parsers.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

public class GameContentStore {

    private Context context;

    public GameContentStore(Context context){

        this.context = context;

    }

    public ArrayList<Word> getContent(String theme, String lang){

        try {
            ArrayList<Word> list = new ArrayList<Word>();
            JSONObject object = this.getJson();
            JSONArray array = object.getJSONArray(theme);
            for(int i=0;i<array.length();i++){
                JSONObject wordJson = array.getJSONObject(i);
                JSONObject translation = wordJson.getJSONObject(lang);
                JSONObject amazigh = wordJson.getJSONObject("Amazigh");
                Word word = new Word();
                word.setWord(amazigh.getString("word"));
                word.setTranslation(translation.getString("word"));
                word.setLang(lang);
                word.setPicuture(translation.getString("photo"));
                word.setSound(translation.getString("sound"));
                word.setAnswer(translation.getString("word"));
                list.add(word);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public ArrayList<Theme> getThemes(){

        try{
            ArrayList<Theme> arrayList = new ArrayList<Theme>();
            JSONObject object = this.getJson();
            for(int i=0;i<object.names().length();i++){
                Theme theme = new Theme();
                theme.setName(object.names().getString(i));
                arrayList.add(theme);
            }
            return arrayList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private JSONObject getJson(){
        try {
            InputStream is = context.getResources().openRawResource(R.raw.content);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            JSONParser parser = new JSONParser();
            JSONObject object = parser.getObject(json);
            return object;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
