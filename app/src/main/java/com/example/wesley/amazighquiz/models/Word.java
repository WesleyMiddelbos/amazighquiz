package com.example.wesley.amazighquiz.models;

public class Word {

    private String word;
    private String translation;
    private String lang;
    private String answer;
    private String picture;
    private String sound;

    public void setWord(String word){
        this.word = word;
    }

    public void setTranslation(String translation){
        this.translation = translation;
    }

    public void setLang(String lang){
        this.lang = lang;
    }

    public void setAnswer(String answer){
        this.answer = answer;
    }

    public void setPicuture(String picture){ this.picture = picture; }

    public void setSound(String sound){ this.sound = sound; }

    public String getWord(){
        return this.word;
    }

    public String getTranslation(){
        return this.translation;
    }

    public String getLang(){
        return this.lang;
    }

    public String getAnswer(){
        return this.answer;
    }

    public String getSound() { return this.sound; }

    public String getPicture() { return this.picture; }

}