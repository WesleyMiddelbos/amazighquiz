package com.example.wesley.amazighquiz.stores;

import android.content.Context;
import android.media.MediaPlayer;
import com.example.wesley.amazighquiz.models.Word;

public class SoundStore {

    private Context context;
    private MediaPlayer mp;

    public SoundStore(Context context){
        this.context = context;
        this.mp = null;
    }

    private static int getStringIdentifier(Context context, String name) {
        return context.getResources().getIdentifier(name, "raw", context.getPackageName());
    }

    private void stopSound(){
            this.mp.stop();
            this.mp.release();
            this.mp = null;
    }

    public void playSound(){
        try {
            Word word = new Word();

            if (this.mp != null){
                stopSound();
                playSound();
            }

            this.mp = MediaPlayer.create(this.context, getStringIdentifier(this.context, word.getSound()));
            mp.start();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
