package com.example.wesley.amazighquiz.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.adapters.SlideAdapter;
import com.example.wesley.amazighquiz.models.Game;
import com.example.wesley.amazighquiz.stores.GameStore;
import com.example.wesley.amazighquiz.stores.SoundStore;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button oefen = findViewById(R.id.oefenBtn);
        Button speel = findViewById(R.id.speelBtn);
        Button over = findViewById(R.id.overBtn);

        oefen.setOnClickListener(this);
        speel.setOnClickListener(this);
        over.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.oefenBtn:
                Intent intentExercise = new Intent(getApplicationContext(), Category2Activity.class);
                startActivity(intentExercise);
                break;
            case R.id.speelBtn:
                Intent intentPlay = new Intent(getApplicationContext(), Category2Activity.class);
                startActivity(intentPlay);
                break;
            case R.id.overBtn:
                Intent intentOver = new Intent(getApplicationContext(),AboutActivity.class);
                startActivity(intentOver);
                break;
        }
    }
}
