package com.example.wesley.amazighquiz.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.wesley.amazighquiz.R;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Button back = findViewById(R.id.BackBtn);

        back.setOnClickListener(this);

    }
    @Override
    public void onClick (View view){
        switch (view.getId()) {
            case R.id.BackBtn:
                Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentBack);
                break;
        }
    }


}
