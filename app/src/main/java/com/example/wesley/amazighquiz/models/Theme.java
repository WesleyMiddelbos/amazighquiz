package com.example.wesley.amazighquiz.models;


public class Theme {

    private String name;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

}
