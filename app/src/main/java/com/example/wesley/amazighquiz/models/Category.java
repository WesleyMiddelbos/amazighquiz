package com.example.wesley.amazighquiz.models;

public class Category {

    private int id;
    private int image;
    private String category;

    public Category(int image, String category) {
        this.image = image;
        this.category = category;
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public String getCategory() {
        return category;
    }
}
