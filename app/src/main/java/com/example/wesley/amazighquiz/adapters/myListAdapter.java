package com.example.wesley.amazighquiz.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.activities.PracticeActivity;
import com.example.wesley.amazighquiz.models.Category;

import java.util.List;

@SuppressWarnings("ALL")
public class myListAdapter extends ArrayAdapter<Category> {

    List<Category> catList;

    Context context;

    int resource;

    public myListAdapter (Context context, int resource, List<Category> catList) {
        super(context, resource, catList);
        this.context = context;
        this.resource = resource;
        this.catList = catList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(resource, null, false);

        ImageView imageView = view.findViewById(R.id.imageView);
        TextView textViewCategory = view.findViewById(R.id.textViewCategory);
        Button buttonSelect = view.findViewById(R.id.buttonSelect);

        Category category = catList.get(position);

        imageView.setImageDrawable(context.getResources().getDrawable(category.getImage()));
        textViewCategory.setText(category.getCategory());
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testIntent = new Intent(context.getApplicationContext(), PracticeActivity.class);
                context.startActivity(testIntent);
                //                removeCatergory(position);
            }
        });
        return view;
    }

    private void removeCatergory(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to remove the category?");

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                catList.remove(position);

                notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
