package com.example.wesley.amazighquiz.models;

import java.util.List;

public class Game {

    private Theme theme;
    private List<Word> words;

    public void setTheme(Theme theme){
        this.theme = theme;
    }

    public void setWord(Word word){
        this.words.add(word);
    }

    public void setWords(List<Word> words){
        this.words = words;
    }

    public Theme getTheme(){
        return this.theme;
    }

    public List<Word> getWords(){
        return this.words;
    }

}
