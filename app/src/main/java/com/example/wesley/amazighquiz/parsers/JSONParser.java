package com.example.wesley.amazighquiz.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    public JSONObject getObject(String input){

        try {
            JSONObject object = new JSONObject(input);

            return object;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public JSONArray getArray(JSONObject object, String element){

        try{
            JSONArray array = object.getJSONArray(element);
            return array;
        }catch (JSONException e){
            e.printStackTrace();
            return null;
        }

    }

}
