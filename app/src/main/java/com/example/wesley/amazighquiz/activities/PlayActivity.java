package com.example.wesley.amazighquiz.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.wesley.amazighquiz.R;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        Button terugBtn = findViewById(R.id.backBtn);

        terugBtn.setOnClickListener(this);

    }

    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                Intent backIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(backIntent);
                break;
        }
    }

}

