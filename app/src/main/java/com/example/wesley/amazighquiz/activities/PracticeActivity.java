package com.example.wesley.amazighquiz.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.View;
import android.widget.Button;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.adapters.SlideAdapter;


public class PracticeActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private SlideAdapter slideAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        Button back = findViewById(R.id.returnBtn);

        back.setOnClickListener(this);

//        viewPager = (ViewPager) findViewById(R.id.);
//        slideAdapter = new SlideAdapter(this);
//        viewPager.setAdapter(slideAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.returnBtn:
                Intent startIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(startIntent);
                break;
        }
    }
}
