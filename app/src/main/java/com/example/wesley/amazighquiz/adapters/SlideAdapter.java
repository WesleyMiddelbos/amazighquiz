package com.example.wesley.amazighquiz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.models.Game;
import com.example.wesley.amazighquiz.models.Word;
import com.example.wesley.amazighquiz.stores.GameStore;

import java.util.List;

public class SlideAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater inflater;

    public SlideAdapter(Context context){
        this.context = context;
    }

    private List<String> getImages(){
        GameStore gameStore = new GameStore(this.context);
        Game game = gameStore.createGame("Dieren01", "Ned");
        List<Word> words = game.getWords();
        List<String> images = null;
        for (int i = 0; i<words.size(); i++) {
            Word word = words.get(i);
            images.add(word.getPicture());
        }
        return images;
    }

    private List<String> getTitlesDefault(){
        GameStore gameStore = new GameStore(this.context);
        Game game = gameStore.createGame("Dieren01", "Ned");
        List<Word> words = game.getWords();
        List<String> titles = null;
        for (int i = 0; i<words.size(); i++) {
            Word word = words.get(i);
            titles.add(word.getWord());
        }
        return titles;
    }

    private List<String> getTitlesAmazigh(){
        GameStore gameStore = new GameStore(this.context);
        Game game = gameStore.createGame("Dieren01", "Ned");
        List<Word> words = game.getWords();
        List<String> titles = null;
        for (int i = 0; i<words.size(); i++) {
            Word word = words.get(i);
            titles.add(word.getAnswer());
        }
        return titles;
    }

    private List<Integer> getBackgroundColors(){
        List<Integer> backgroundColors = null;
        backgroundColors.add(R.color.flagBlue);
        backgroundColors.add(R.color.flagGreen);
        backgroundColors.add(R.color.flagRed);
        backgroundColors.add(R.color.flagYellow);
        return backgroundColors;
    }

    private List<Integer> backgroundColors = getBackgroundColors();
    private List<String> images = getImages();
    private List<String> titlesAmazigh = getTitlesAmazigh();
    private List<String> titlesDefault = getTitlesDefault();

    @Override
    public int getCount() {
        return getImages().size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_practice, container,false);
        LinearLayout layoutslide = view.findViewById(R.id.slideLayout);
        ImageView imgslide = view.findViewById(R.id.slideImage);
        TextView titleDefault = view.findViewById(R.id.slideTextDefault);
        TextView titleAmazigh = view.findViewById(R.id.slideTextAmazigh);
        layoutslide.setBackgroundColor(backgroundColors.get(position));
        imgslide.setImageResource(Integer.parseInt(images.get(position)));
        titleDefault.setText(titlesDefault.get(position));
        titleAmazigh.setText(titlesAmazigh.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
