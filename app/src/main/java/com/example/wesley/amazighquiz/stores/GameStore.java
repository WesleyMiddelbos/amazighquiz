package com.example.wesley.amazighquiz.stores;

import android.content.Context;
import android.content.res.Resources;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.models.Game;
import com.example.wesley.amazighquiz.models.Theme;
import com.example.wesley.amazighquiz.models.Word;
import com.example.wesley.amazighquiz.parsers.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameStore {

    private JSONParser jParser;
    private Context context;

    public GameStore(Context context){
        this.context = context;
    }

    public Game createGame(String theme, String lang){
        GameContentStore store = new GameContentStore(this.context);
        List<Word> words = store.getContent(theme,lang);
        Game game = new Game();
        Theme themeModel = new Theme();
        themeModel.setName(theme);
        game.setWords(words);
        game.setTheme(themeModel);
        return game;
    }

    public Boolean selectWord(Word word, String selected){
        if(word.getWord().equals(selected)){
            return true;
        }
        return false;
    }


}
