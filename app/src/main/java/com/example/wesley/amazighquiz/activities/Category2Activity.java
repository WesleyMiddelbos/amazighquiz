package com.example.wesley.amazighquiz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.adapters.myListAdapter;
import com.example.wesley.amazighquiz.models.Category;
import com.example.wesley.amazighquiz.models.Theme;
import com.example.wesley.amazighquiz.stores.GameContentStore;

import java.util.ArrayList;
import java.util.List;

public class Category2Activity extends AppCompatActivity implements View.OnClickListener {
        List<Category> catList;

        ListView listView;
        private Button BackButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category2);
        catList = new ArrayList<>();
        listView = (ListView)findViewById(R.id.listView);
        ArrayList<Theme> themes = new GameContentStore(this).getThemes();

        for(int i=0;i<themes.size();i++){
            catList.add(new Category(R.drawable.splashscreen, themes.get(i).getName()));
        }

        myListAdapter adapter = new myListAdapter(this, R.layout.custom_list, catList);

        listView.setAdapter(adapter);

        this.BackButton = findViewById(R.id.backBtnCategory);

        this.BackButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtnCategory:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
        }
    }
}
