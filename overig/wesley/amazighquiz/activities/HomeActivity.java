package com.example.wesley.amazighquiz.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.wesley.amazighquiz.R;
import com.example.wesley.amazighquiz.stores.GameStore;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        GameStore store = new GameStore(this);

        store.createGame("Dieren 01","Ned");
    }
}
